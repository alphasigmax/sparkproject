#!/usr/bin/env bash
rm -rf shotenengine-0.0.1-SNAPSHOT/
git pull
 ./gradlew clean assemble
tar -xvf ./build/distributions/shotenengine-0.0.1-SNAPSHOT.tar
nohup ./shotenengine-0.0.1-SNAPSHOT/bin/shotenengine &
tail -f nohup.out
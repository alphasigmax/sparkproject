package common;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.spark.sql.SparkSession;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by srramas on 1/28/17.
 */
public class SampleRecommender {


    public static void main(String a[]) throws IOException, TasteException {
       /* System.out.println("hello");
        DataModel model = new FileDataModel(new File("/mnt/haggell/bigdata/output/xproductClassification/dataset.csv"));

       Collection<GenericItemSimilarity.ItemItemSimilarity> correlations =new ArrayList<GenericItemSimilarity.ItemItemSimilarity>();
        correlations.add(new GenericItemSimilarity.ItemItemSimilarity(1, 2, 0.5));
        correlations.add(new GenericItemSimilarity.ItemItemSimilarity(1, 2, -0.95));
        correlations.add(new GenericItemSimilarity.ItemItemSimilarity(2, 3, 0.75));



        ItemSimilarity itemSimilarity =
                new GenericItemSimilarity(correlations);

        Recommender recommender =
                new GenericItemBasedRecommender(model, itemSimilarity);
        Recommender cachingRecommender = new CachingRecommender(recommender);
        List<RecommendedItem> recommendations = cachingRecommender.recommend(1, 3);
        for (RecommendedItem recommendation : recommendations) {
            System.out.println(recommendation);
        }*/


        //  DataModel datamodel = new FileDataModel(new File("./src/main/resources/mahoutsim.txt"));
        try {
            //Creating data model
//            DataModel datamodel = new FileDataModel(new File("./src/main/resources/recom_spark.txt")); //data


//            DataModel datamodel = new ParquetFileDataModel(new File("/data/haggell/bigdata/output/similar/part-00000-72b28e4e-fb88-4ab8-9e13-9ac263288540.snappy.parquet")); //data
            DataModel     datamodel = new ParquetFileDataModel(new File("./src/main/resources/mydata.parquet")); //data

            //Creating UserSimilarity object.
            UserSimilarity usersimilarity = new PearsonCorrelationSimilarity(datamodel);

            //Creating UserNeighbourHHood object.
            UserNeighborhood userneighborhood = new NearestNUserNeighborhood(2, usersimilarity, datamodel);
                    //ThresholdUserNeighborhood(0.1, usersimilarity, datamodel);

            //Create UserRecomender
            UserBasedRecommender recommender = new GenericUserBasedRecommender(datamodel, userneighborhood, usersimilarity);
         for(int i=0;i<30;i++) {

             System.out.println("\n\nFor user " + i);
             System.out.println("Most Similar Users are: ") ;
             for(long l:recommender.mostSimilarUserIDs(i,2)) {
                 System.out.println(l);
             }

             List<RecommendedItem> recommendations = recommender.recommend(i, 3);

             System.out.println("Recommended items are : ") ;
               for (RecommendedItem recommendation : recommendations) {
                   System.out.println(recommendation);
               }
           }

        } catch (Exception e) {

            System.out.println("error :" + e);
            e.printStackTrace();
        }

    }


}

package indexers


import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

import scala.collection.mutable.HashMap
import scala.collection.{Map, mutable}

/**
  * Created by spyder on 11/10/16.
  */
object TFIDFIndexer {


  /**
    * Returns a document-term matrix where each element is the TF-IDF of the row's document and
    * the column's term.
    */
  def documentTermMatrix(docs: DataFrame, stopWords: Set[String], numTerms: Int,
                         spark: SparkSession): (RDD[Vector], Map[Int, String], RDD[(Long, String)], Map[String, Double]) = {


    import spark.implicits._
    // if it already exists then we load from FS insteading of calculating everything
    val sc = spark.sparkContext
    val conf = spark.sparkContext.hadoopConfiguration
    val fs = org.apache.hadoop.fs.FileSystem.get(conf)
    val exists = false
    //

    var docTermFreqs: RDD[(String, HashMap[String, Int])] = null

    if (exists && fs.exists(new org.apache.hadoop.fs.Path("/shoten/output/doctermfreq/"))) {
      // This doesn't work. We can't load from parquet due to scala misunderstandings
      val docTermFreqsrdd = spark.read.parquet("/shoten/output/doctermfreq")


    }
    else {
      docTermFreqs = docs.rdd.map(row => {
        (row.getString(0), row.getSeq[String](1))
      }).mapValues(allterms => {

        val termFreqsInDoc: HashMap[String, Int] = allterms.foldLeft(new HashMap[String, Int]()) {

          (map, term) => {
            map += term -> (map.getOrElse(term, 0) + 1)
          }
        }
        // This is the term frequency for each individual document.
        termFreqsInDoc
      })
      /*
      THis is the document => word frequency mapping.
      For example:
      doc1( word1 -> 5, word2 -> 7, word3 -> 1)
       */

      // docTermFreqs.map(attributes => DocFreq(attributes._1, attributes._2)).toDF("title", "wordfrequencies").write.mode(SaveMode.Overwrite).parquet("/shoten/output/doctermfreq")
    }

    val docsTerms = docTermFreqs.flatMap {

      case (s: String, hashMap: mutable.HashMap[String, Int]) =>
        val docTotal = hashMap.values.sum
        hashMap.map {
          case (word: String, count: Int) =>
            (s, word, count, docTotal)
        }
    }.toDF("title", "docword", "wordcount", "totalwords")


    //  docsTerms.write.parquet("/shoten/output/doctermfreqexp")
    //docTermFreqs.cache()
    // We are good till here


    val docFreqs = documentFrequenciesDistributed(docTermFreqs.map(_._2))
    // println("Number of terms: " + docFreqs.count())


    //create unique document id (in this case an integer for every wikipedia title)

    val docIds = docTermFreqs.map(_._1).zipWithUniqueId().map(_.swap)
    val numDocs = docIds.count()
    //  println(" Total number of documents =" + numDocs)
    // Now we have UniqueID, Title in docIds
    // docIds.toDF("docid", "doc_title").write.mode(SaveMode.Overwrite).parquet("/shoten/output/docids")


    // This calculates the inverse document frequence of all terms
    // term => idfs score
    val idfScore = inverseDocumentFrequencies(docFreqs, numDocs)


    // Maps terms to their indices in the vector
    // get all the terms and put an index on them
    val idTerms = idfScore.keys.zipWithIndex
    //idTerms.toDF("word","uid").write.mode(SaveMode.Overwrite).parquet("/shoten/output/idterms")

    val _idfScore = idfScore.toDF("word", "idfscore")
    val _idTerms = idTerms.toDF("oldword", "wordid")
    val idfScoreIndex = _idfScore.join(_idTerms, _idfScore.col("word") === _idTerms.col("oldword")).drop("oldword").toDF("word", "idfscore", "wordid")
    //  idfScoreIndex.write.mode(SaveMode.Overwrite).parquet("/shoten/output/idfscoreindex")
    val totalTerms = idfScoreIndex.count()
    //WORKING here
    /*


    idfScoreIndex([Word, IDFScore], WordID)

    RESULT (idTerms.Word, idfScore.IdfScore * docTermFreqs.count/docTotal)

(bIdTerms(term), bIdfs(term) * termFreqs(term) / docTotalTerms)


   combine

     */

    val BigData = idfScoreIndex.join(docsTerms, idfScoreIndex.col("word") === docsTerms.col("docword")).drop("docword")
    BigData.write.parquet("/shoten/output/bigdata")

      //Window for each Title and calculate vector for each title

    // We want a vector of this
//    (idfScoreIndex.ID, idfScoreIndex.IdfScore * docsTerms.wordCount/docsTerms.totalwords)

    val w = Window.partitionBy("title").orderBy(col("word").desc)

    def makeDT(idfscore: Column): Column = {
      val a: Array[Int] = new Array(10)
      val b: Array[Double] = new Array(10)
      Vectors.sparse(10, a, b)
      idfscore + totalTerms
    }


    val newdf = BigData.select(BigData("*"), sum(BigData("wordcount")).over(w), makeDT(BigData("idfscore")))

    //document and frequency of each word. we take (word=> number of times) thru map


    /*&
        val vecs = docTermFreqs.map(_._2).map(termFreqs => {
          val docTotalTerms = termFreqs.values.sum
          val termScores = termFreqs.filter {
            case (term, freq) => bIdTerms.contains(term)
          }.map{
            case (term, freq) => (bIdTerms(term), bIdfs(term) * termFreqs(term) / docTotalTerms)
          }.toSeq
          Vectors.sparse(bIdTerms.size, termScores)
        })

     */

    //    val termIds = idTerms.map(_.swap)
    //  (vecs, termIds, docIds, idfs)
    null
  }


  def documentFrequenciesDistributed(docTermFreqs: RDD[HashMap[String, Int]])
  : RDD[(String, Int)] = {

    /*
    We have passed in RDD of term frequencies per document till here. So each row has
    word: number of times it appears in this document
     */

    /*
    1) get the key/word of all elements across all rows,
    2) create a tuple with key (string/word) and count as 1
    3) ??
     */

    // docTermFreqs.flatMap(_.keySet).map((_, 1)).reduceByKey(_ + _, 15).toDF().write.parquet("/shoten/output/reduced")
    // we reduce by key (word) and number of times across documents. 15 is number of partitions
    val docFreqs = docTermFreqs.flatMap(_.keySet).map((_, 1)).reduceByKey(_ + _, 15)
    // we skip the ordering and return the whole document
    // val ordering = Ordering.by[(String, Int), Int](_._2)
    //docFreqs.top(numTerms)(ordering)
    docFreqs
  }

  def inverseDocumentFrequencies(docFreqs: RDD[(String, Int)], numDocs: Long)
  : RDD[(String, Double)] = {
    docFreqs.map { case (term, count) => (term, math.log(numDocs + 1 / count + 1)) }
  }
}

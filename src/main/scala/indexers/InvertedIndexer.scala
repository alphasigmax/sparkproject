package indexers

import java.io.File

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.DataFrame
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by spyder on 5/09/16.
  *
  * This class creates an inverted index for all the documents present in the input folder
  */



object InvertedIndexer {
  // change this to the place where the inputs and outputs exist. Or even to HDFS path
  val datadir: String = "/media/spyder/BitEast/project/shoten/"
  val name = "InvertedIndexer"
  val conf = new SparkConf().
    setMaster("local").
    setAppName(name).
    set("spark.app.id", name)

  var sc: SparkContext= new SparkContext(conf)

  def main(args: Array[String]): Unit = {
    println("Starting the Spark Scala indexing job ...")
    // To silence Metrics warning.
    System.out.println("Spark Initialization complete")
    deleteDir(new File(datadir + "output/3"))
    val startTime = System.currentTimeMillis()
    val javapairrdd: RDD[(String, String)] = sc.wholeTextFiles(datadir + "input/*")

    val dataframe = createInvertedIndex(javapairrdd)

    dataframe.show(false)
    dataframe.write.parquet(datadir + "output/3")

    val endtime = System.currentTimeMillis()
    println(s"completed mapping in ${endtime - startTime} seconds")


  }

  case class X(callId: String, oCallId: String)

  def createInvertedIndex(javapairrdd: RDD[(String, String)]): DataFrame = {

    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    val df: RDD[(String, String)] = javapairrdd.map {
      case (id, text) =>
        //  println(s" id =$id  & text = $text")
        (id, text)
    }.flatMap {
      case (path, text) =>
        println(s" path of file =$path  & nextmaptext = ${text.substring(0, 25)}")
        text.trim.split("""[^\p{IsAlphabetic}]+""").map(word => {
          println(s" split into workd $word  ")
          val xword = word.toLowerCase()
          (xword, path)
        }

        )
    }.map {
      // We're going to use the (word, path) tuple as a key for counting
      // all of them that are the same. So, create a new tuple with the
      // pair as the key and an initial count of "1".
      case (word, path) => {
        println(s"word $word is in path $path")
        ((word, path), 1)
      }
    }.reduceByKey { _ + _ }.map {
      // Rearrange the tuples; word is now the key we want.
      case ((word, path), n) => (word, (path, n))
    }.groupByKey.mapValues(iterator => iterator.mkString(", "))
    //.saveAsTextFile(datadir + "output/3")

    df.map {
      case (s0, s1) => X(s0, s1)
    }.toDF()

  }

  def deleteDir(file: File): Unit = {
    var contents = file.listFiles()
    if (contents != null) {
      contents.foreach {
        content =>
          deleteDir(content)
      }
    }
    file.delete()
  }
}

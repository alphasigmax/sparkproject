//package nlp
//
//import java.util.Properties
//
//import edu.stanford.nlp.ling.CoreAnnotations.{LemmaAnnotation, SentencesAnnotation, TokensAnnotation}
//import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
//
//import scala.collection.Map
//import scala.collection.mutable.ArrayBuffer
//import scala.collection.mutable.HashMap
//import scala.collection.JavaConverters._
///**
//  * Created by spyder on 11/10/16.
//  */
//object StanfordNLP {
//
//  def createNLPPipeline(): StanfordCoreNLP = {
//    val props = new Properties()
//    props.put("annotators", "tokenize, ssplit, pos, lemma")
//    new StanfordCoreNLP(props)
//  }
//
//
//  def plainTextToLemmas(text: String, stopWords: Set[String], pipeline: StanfordCoreNLP)
//  : Seq[String] = {
//    //get the whole text of this document
//    val doc = new Annotation(text)
//    //annotate the whole document using stanford annotations
//    pipeline.annotate(doc)
//    // this is lemmatization. We can either use stemming or lemmatization (which one is preferable will be decided later)
//
//    val lemmas = new ArrayBuffer[String]()
//    val sentences = doc.get(classOf[SentencesAnnotation])
//    //break down the text into sentences and then each sentence in tokens(words)
//    for (sentence <- sentences.asScala;
//         token <- sentence.get(classOf[TokensAnnotation]).asScala) {
//      //for each word we get the lemma and then we build new sentence using this lemmatized word
//      val lemma = token.get(classOf[LemmaAnnotation])
//      //not sure if we need all this check. We just need to check the stopwords maybe?
//      if (lemma.length > 2 && !stopWords.contains(lemma) && isOnlyLetters(lemma)) {
//        // we add it to the array buffer of new word
//        lemmas += lemma.toLowerCase
//      }
//    }
//    lemmas
//  }
//
//  def isOnlyLetters(str: String): Boolean = {
//    // While loop for high performance
//    var i = 0
//    while (i < str.length) {
//      if (!Character.isLetter(str.charAt(i))) {
//        return false
//      }
//      i += 1
//    }
//    true
//  }
//
//  def loadStopWords(path: String) = scala.io.Source.fromFile(path).getLines().toSet
//}

package sqlreaders

/**
  * Created by spyder on 28/10/16.
  */


import java.util.concurrent.atomic.LongAccumulator

import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{SaveMode, SparkSession}

object DatabaseReader {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder().master("local[*]")
      //.config("spark.ui.port", "4070")
      .appName("Shoten: Database Reader")
      .getOrCreate()


    val similarProductsQuery = "(select a.id,a.asin,c.similar_products_id from amazon_product a, amazon_product_similarity b, amazon_product_similarity_similar_products c where a.id = b.amazon_product_id and b.id=c.amazon_product_similarity_id) as amazonsimilar"

    val amazonProducts = "amazon_product"
    val orderlog = "order_log"

    //change the table to match

    val jdbcDF = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://10.0.0.4:3306/table?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&us  eSSL=false")
      .option("dbtable", amazonProducts)
      .option("user", "root")
      .option("password", "")
      .load()
    val xjdbcDF = jdbcDF.withColumn("loc", concat(lit("https://www.aihello.com/p/a/"), jdbcDF.col("asin"))).withColumn("changefreq", lit("monthly")).select("loc", "changefreq")
    //.select(jdbcDF.col("asin").as("MainPart"))
    val arrays = Array.fill[Double](195)(1)
    //.sample(false,0.000002)
val dfs = xjdbcDF.randomSplit(arrays)

    for(i <- dfs.indices){
      println("i is: " + i)
           val path: String = "/data/aihello/sitemap/sitemap" + i + ".xml"
      dfs(i).write.format("com.databricks.spark.xml")
        .option("rootTag", "urlset")
        .option("rowTag", "url").mode(SaveMode.Overwrite)
        .save(path)

    }




    //
    /*
    <url>
<loc>https://www.aihello.com/p/a/B01LXSHRCW?pdet=iPhone_7_Luxury_Flip_Case__Drop_Protection_Canvas_Diary_Wood_skin_design_Wa </loc>
<image:image>
<image:loc> https://images-na.ssl-images-amazon.com/images/I/4177Jzc7mSL._SL75_.jpg</image:loc>
<image:caption>Photo of iPhone 7 Luxury Flip Case, Drop Protection Canvas Diary Wood skin design Wa</image:caption>
</image:image>
<changefreq>monthly</changefreq>
</url>
     */
    //    val jsondf = spark.read.json("/media/spyder/mkultra/bigdata/newreviews/backup/meta_Country.json")
    //
    //    val cutdf = jsondf.select(jsondf("asin"), jsondf("imUrl").as("image_url"),jsondf("price"))
    //    jsondf.write.mode(SaveMode.Overwrite).format("jdbc")
    //          .option("url", "jdbc:mysql://localhost:3306/table?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false")
    //          .option("dbtable", "newamazonproduct")
    //          .option("user", "root")
    //          .option("password", "").save()
    //
    //    jsondf.show(false)

    // jdbcDF.write.parquet("/data/haggell/bigdata/productsimilar")


  }


}

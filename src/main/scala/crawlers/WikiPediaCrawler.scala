//package crawlers
//
//import java.io.File
//
//import edu.stanford.nlp.pipeline.StanfordCoreNLP
//import edu.stanford.nlp.util.PropertiesUtils
//import org.apache.spark.rdd.RDD
//import org.apache.spark.{SparkConf, SparkContext}
//
//
//// For implicit conversions from RDDs to DataFrames
//
//
///**
//  * Created by spyder on 5/09/16.
//  *
//  * This class creates an inverted index for all the documents present in the input folder
//  */
//
//
//object WikiPediaCrawler {
//  // change this to the place where the inputs and outputs exist. Or even to HDFS path
//  val datadir: String = "/media/spyder/BitEast/project/shoten/"
//  val name = "LocalCrawler"
//  val conf = new SparkConf().
//    setMaster("local").
//    setAppName(name).
//    set("spark.app.id", name)
//
// val pipeline: StanfordCoreNLP = new StanfordCoreNLP(PropertiesUtils.asProperties("annotators", "tokenize,ssplit,pos,lemma,ner,parse,natlog", "ssplit.isOneSentence", "false", "parse.model", "edu/stanford/nlp/models/srparser/englishSR.ser.gz", "ner.model", "edu/stanford/nlp/models/ner/english.all.3class.caseless.distsim.crf.ser.gz", "tokenize.language", "en"))
//
//  var sc: SparkContext = new SparkContext(conf)
//  val sqlContext = new org.apache.spark.sql.SQLContext(sc)
//
//  import sqlContext.implicits._
//
//  def main(args: Array[String]): Unit = {
//    println("Starting the Spark Scala Local Crawler job ...")
//    // To silence Metrics warning.
//    System.out.println("Spark Initialization complete")
//    deleteDir(new File(datadir + "output/raw"))
//    val startTime = System.currentTimeMillis()
//    val javapairrdd: RDD[(String, String)] = sc.wholeTextFiles(datadir + "input/*")
//
//    val dataframe = crawlLocation(javapairrdd)
//
//    dataframe.show(false)
//    dataframe.write.parquet(datadir + "output/raw")
//
//    javapairrdd.map{
//      case (siteId, siteText) =>{
//        println ("siteText  is $siteText")
//
//      }
//    }
//
//    val endtime = System.currentTimeMillis()
//    println(s"completed mapping in ${endtime - startTime} seconds")
//
//
//  }
//
//  case class Puddle(siteId: String, siteText: String)
//
//  def crawlLocation(javapairrdd: RDD[(String, String)]) = {
//    javapairrdd.map { case (s0, s1) => Puddle(s0, s1) }.toDF()
//  }
//
//  def cleanText(text:String)={
//
//
//  }
//
//  //ONLY for DEBUG
//  def deleteDir(file: File): Unit = {
//    val contents = file.listFiles()
//    if (contents != null) {
//      contents.foreach {
//        content =>
//          deleteDir(content)
//      }
//    }
//    file.delete()
//  }
//}

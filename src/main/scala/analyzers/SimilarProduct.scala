package analyzers

import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{CountVectorizer, StringIndexer}
import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
  * Created by spyder on 27/01/17.
  */
object SimilarProduct {


  def main(args: Array[String]): Unit = {
    Logger.getRootLogger.setLevel(Level.WARN)
    val spark = SparkSession
      .builder().master("local[*]")
      .config("spark.ui.port", "4080")
      .appName("Shoten: Database Reader")
      .getOrCreate()

    import spark.implicits._
    var df = spark.read.parquet("/data/haggell/bigdata/output/xproductClassification")
    val stringindexer = new StringIndexer()
      .setInputCol("asin")
      .setOutputCol("user")
    val modelc = stringindexer.fit(df)
    df = modelc.transform(df)
    df.orderBy("asin").show(false)
val xdf=df.select("user","topicIndex","weight")

  val ydf=  xdf.map{
      row=>{

        (row.getDouble(0).toInt, row.getInt(1), row.getDouble(2) )
      }
    }.toDF("userid","itemid","prefix")
ydf.sort("userid").coalesce(1).write.mode(SaveMode.Overwrite).parquet("/data/haggell/bigdata/output/similar/")
    ydf.show(false)
    // Finished data massaging

//    val als = new ALS()
//      .setMaxIter(5)
//      .setRegParam(0.01)
//      .setUserCol("user")
//      .setItemCol("topicindex")
//      .setRatingCol("weight")
//    val model = als.fit(df)
//
//
//    println("user factors")
//    val userf= model.userFactors
//    userf.show(false)
//
//    println("item factors")
//    val itemf = model.itemFactors
//    itemf.show(false)
//
//    userf.printSchema()
//
//    userf.map {
//      row => {
//        println(row.getInt(0))
//        println("second " + row.get(1))
//        (row.getInt(0))
//      }
//    }.count()
//
//    val predictions = model.transform(df)
//    predictions.show(false)

    //    val evaluator = new RegressionEvaluator()
    //      .setMetricName("rmse")
    //      .setLabelCol("weight")
    //      .setPredictionCol("prediction")
    //    val rmse = evaluator.evaluate(predictions)
    //    println(s"Root-mean-square error = $rmse")
  }
}

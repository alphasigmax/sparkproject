//package analyzers
//
///**
//  * Created by spyder on 24/09/16.
//  */
//
//import java.io.File
//
//import breeze.linalg.{DenseMatrix => BDenseMatrix, DenseVector => BDenseVector, SparseVector => BSparseVector}
//import nlp.StanfordNLP._
//import objects.WikiLematized
//import org.apache.spark.ml.feature.{HashingTF, IDF}
//import org.apache.spark.ml.linalg.SparseVector
////import org.apache.spark.ml.linalg.Vector
//import org.apache.spark.mllib.linalg.{VectorUDT, _}
//import org.apache.spark.mllib.linalg.distributed.RowMatrix
//import org.apache.spark.rdd.RDD
//import org.apache.spark.sql.types.{DoubleType, StringType, StructType}
//import org.apache.spark.sql.{DataFrame, SparkSession}
//import parsers.WikipediaParser._
//
//import scala.collection.Map
//import scala.collection.mutable.ArrayBuffer
//
//
//object LatentSemanticAnalyzer {
//
//  val schema = new StructType()
//    .add("label", DoubleType)
//    .add("features", new VectorUDT())
//
//
//  val docSchema = new StructType()
//    .add("title", StringType)
//    .add("document", StringType)
//
//
//  def main(args: Array[String]) {
//    val k = if (args.length > 0) args(0).toInt else 100
//    val numTerms = if (args.length > 1) args(1).toInt else 50000
//    val sampleSize = if (args.length > 2) args(2).toDouble else 0.0001
//
//    val spark = SparkSession
//      .builder().master("local[*]")
//      .appName("Shoten: Latent Semantic Analysis")
//      .getOrCreate()
//    //  invertedIndex(sampleSize, spark)
//    val termDocMatrix = preprocessing(sampleSize, numTerms, spark)
//    println(s" schema is " + termDocMatrix.schema)
//    //
//    //
//    val rows: RDD[Vector] = termDocMatrix.rdd.map { row => {
//      val v: SparseVector = row.getAs[SparseVector]("features")
//      Vectors.fromML(v.compressed)
//    }
//      }
//
//   val total= rows.count()
//    println("total is $total")
//      //termDocMatrix.toDF(schema).write.parquet("/shoten/wikiplain/")
//
//
//          val mat = new RowMatrix(rows)
//          val svd = mat.computeSVD(k, computeU = true)
//
//         println("Singular values: " + svd.s)
//      // Save and load model
//
//
//      val topConceptTerms = topTermsInTopConcepts(svd, 10, 10, null)
//    val toptermhead = topConceptTerms.head
//   val firstterm= toptermhead.head._1
//
////       val topConceptDocs = topDocsInTopConcepts(svd, 10, 10, null)
//
//   val topdocs= topDocsForTerm(mat,svd.V, firstterm)
//    topdocs.map(eachrow=>{
//      println (s" Documents are "+eachrow._2)
//      eachrow._1
//    }).size
//      //        val pw = new PrintWriter(new File("/shoten/output/finalresults.txt" ))
//      //        for ((terms, docs) <- topConceptTerms.zip(topConceptDocs)) {
//      //          pw.write("Concept terms: " + terms.map(_._1).mkString(", "))
//      //          pw.write("Concept docs: " + docs.map(_._1).mkString(", "))
//      //
//      //          pw.write("\n")
//      //        }
//      //        pw.close()
//
//    }
//
//
//
//
//    /**
//      * Returns an RDD of rows of the document-term matrix, a mapping of column indices to terms, and a
//      * mapping of row IDs to document titles.
//      */
//    def preprocessing(sampleSize: Double, numTerms: Int, spark: SparkSession)
//    : DataFrame = {
//      //    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
//      //    import sqlContext.implicits._
//      import spark.implicits._
//
//
//      val conf = spark.sparkContext.hadoopConfiguration
//      val fs = org.apache.hadoop.fs.FileSystem.get(conf)
//      var exists = fs.exists(new org.apache.hadoop.fs.Path("/shoten/output/wikiplaindf/"))
//
//
//      var plainText: DataFrame = null
//
//
//      // if we have no data locally then we read wikipedia page and populate our dataframe with it
//      if (!exists) {
//        // create and get wiki dataframe
//        plainText = getWikiData(spark, sampleSize)
//      }
//      else {
//        plainText = spark.read.parquet("/shoten/output/wikiplaindf/")
//
//      }
//      //
//      //    println("total records = "+ plainText.count())
//
//      // We broadcast the stop words to all partition of spark. All partitiions can now access the
//      // variable from any spark slave
//      val stopWords = spark.sparkContext.broadcast(loadStopWords("/shoten/stopwords_eng.txt")).value
//      exists = fs.exists(new org.apache.hadoop.fs.Path("/shoten/output/filtereddf/"))
//      var filtered: DataFrame = null
//      //read from local file system
//      if (exists) {
//        filtered = spark.read.parquet("/shoten/output/filtereddf/")
//      }
//      else // else create from our plaintext dataframe
//      {
//
//        val lemmatized = plainText.mapPartitions(iter => {
//          val pipeline = createNLPPipeline()
//          iter.map { row => {
//            //TODO. create WikiLematized structure here instead of one more map func
//            (row.getString(0), plainTextToLemmas(row.getString(1), stopWords, pipeline))
//          }
//          }
//        })
//        /*
//       The second column in this rdd has a SEQ of lemmatized words. We filter our all records where the doc content is at least 2 words.
//       Single word documents are discarded
//        */
//        filtered = lemmatized.filter(_._2.size > 1).map(attributes => WikiLematized(attributes._1, attributes._2)).toDF("title", "words")
//        filtered.write.parquet("/shoten/output/filtereddf/")
//
//      }
//
//      //filtered : (Document, [Words])
//      var rescaledData: DataFrame = null;
//      exists = fs.exists(new org.apache.hadoop.fs.Path("/shoten/output/featurizedIDF/"))
//      if (exists) {
//        rescaledData = spark.read.parquet("/shoten/output/featurizedIDF/")
//      }
//      else {
//        val hashingTF = new HashingTF()
//          .setInputCol("words").setOutputCol("rawFeatures")
//        val featurizedData = hashingTF.transform(filtered)
//        //  featurizedData.write.mode(SaveMode.Overwrite).parquet("/shoten/output/featurized/")
//        // alternatively, CountVectorizer can also be used to get term frequency vectors
//        val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
//        val idfModel = idf.fit(featurizedData)
//        rescaledData = idfModel.transform(featurizedData)
//        //  rescaledData.write.mode(SaveMode.Overwrite).parquet("/shoten/output/featurizedIDF/")
//      }
//
//      // lets now calculate the tf-idf values
//      //  documentTermMatrix(filtered, stopWords, numTerms, spark)
//
//      // Learn a mapping from words to Vectors.
//      //    val word2Vec = new Word2Vec()
//      //      .setInputCol("words")
//      //      .setOutputCol("result")
//      //       .setMinCount(0)
//      //    val model = word2Vec.fit(rescaledData)
//      //    val word2vec = model.transform(rescaledData)
//      //    word2vec.write.mode(SaveMode.Overwrite).parquet("/shoten/output/word2vec/")
//
//
//      rescaledData
//    }
//
//
//    def topTermsInTopConcepts(svd: SingularValueDecomposition[RowMatrix, Matrix], numConcepts: Int,
//                              numTerms: Int, termIds: Map[Int, String]): Seq[Seq[(Int, Double)]] = {
//      val v = svd.V
//      val topTerms = new ArrayBuffer[Seq[(Int, Double)]]()
//      val arr = v.toArray
//      for (i <- 0 until numConcepts) {
//        val offs = i * v.numRows
//        val termWeights = arr.slice(offs, offs + v.numRows).zipWithIndex
//        val sorted = termWeights.sortBy(-_._1)
//        topTerms += sorted.take(numTerms).map { case (score, id) => (id, score) }
//      }
//
//
////      printToFile(new File("/shoten/output/topterms.txt")) { p =>
////        topTerms.foreach(p.println)
////      }
//
//      topTerms
//    }
//
//    def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
//      val p = new java.io.PrintWriter(f)
//      try {
//        op(p)
//      } finally {
//        p.close()
//      }
//    }
//
//    def topDocsInTopConcepts(svd: SingularValueDecomposition[RowMatrix, Matrix], numConcepts: Int,
//                             numDocs: Int, docIds: Map[Long, String]): Seq[Seq[(Long, Double)]] = {
//      val u = svd.U
//      val topDocs = new ArrayBuffer[Seq[(Long, Double)]]()
//      for (i <- 0 until numConcepts) {
//        val docWeights = u.rows.map(_.toArray(i)).zipWithUniqueId
//        topDocs += docWeights.top(numDocs).map { case (score, id) => (id, score) }
//      }
////      printToFile(new File("/shoten/output/topDocs.txt")) { p =>
////        topDocs.foreach(p.println)
////      }
//
//      topDocs
//    }
//
//    /**
//      * Selects a row from a matrix.
//      */
//    def row(mat: BDenseMatrix[Double], index: Int): Seq[Double] = {
//      (0 until mat.cols).map(c => mat(index, c))
//    }
//
//    /**
//      * Selects a row from a matrix.
//      */
//    def row(mat: Matrix, index: Int): Seq[Double] = {
//      val arr = mat.toArray
//      (0 until mat.numCols).map(i => arr(index + i * mat.numRows))
//    }
//
//    /**
//      * Selects a row from a distributed matrix.
//      */
//    def row(mat: RowMatrix, id: Long): Array[Double] = {
//      mat.rows.zipWithUniqueId.map(_.swap).lookup(id).head.toArray
//    }
//
//    /**
//      * Finds the product of a dense matrix and a diagonal matrix represented by a vector.
//      * Breeze doesn't support efficient diagonal representations, so multiply manually.
//      */
//    def multiplyByDiagonalMatrix(mat: Matrix, diag: Vector): BDenseMatrix[Double] = {
//      val sArr = diag.toArray
//      new BDenseMatrix[Double](mat.numRows, mat.numCols, mat.toArray)
//        .mapPairs { case ((r, c), v) => v * sArr(c) }
//    }
//
//    /**
//      * Finds the product of a distributed matrix and a diagonal matrix represented by a vector.
//      */
//    def multiplyByDiagonalMatrix(mat: RowMatrix, diag: Vector): RowMatrix = {
//      val sArr = diag.toArray
//      new RowMatrix(mat.rows.map(vec => {
//        val vecArr = vec.toArray
//        val newArr = (0 until vec.size).toArray.map(i => vecArr(i) * sArr(i))
//        Vectors.dense(newArr)
//      }))
//    }
//
//    /**
//      * Returns a matrix where each row is divided by its length.
//      */
//    def rowsNormalized(mat: BDenseMatrix[Double]): BDenseMatrix[Double] = {
//      val newMat = new BDenseMatrix[Double](mat.rows, mat.cols)
//      for (r <- 0 until mat.rows) {
//        val length = math.sqrt((0 until mat.cols).map(c => mat(r, c) * mat(r, c)).sum)
//        (0 until mat.cols).map(c => newMat.update(r, c, mat(r, c) / length))
//      }
//      newMat
//    }
//
//    /**
//      * Returns a distributed matrix where each row is divided by its length.
//      */
//    def rowsNormalized(mat: RowMatrix): RowMatrix = {
//      new RowMatrix(mat.rows.map(vec => {
//        val length = math.sqrt(vec.toArray.map(x => x * x).sum)
//        Vectors.dense(vec.toArray.map(_ / length))
//      }))
//    }
//
//    /**
//      * Finds terms relevant to a term. Returns the term IDs and scores for the terms with the highest
//      * relevance scores to the given term.
//      */
//    def topTermsForTerm(normalizedVS: BDenseMatrix[Double], termId: Int): Seq[(Double, Int)] = {
//      // Look up the row in VS corresponding to the given term ID.
//      val termRowVec = new BDenseVector[Double](row(normalizedVS, termId).toArray)
//
//      // Compute scores against every term
//      val termScores = (normalizedVS * termRowVec).toArray.zipWithIndex
//
//      // Find the terms with the highest scores
//      termScores.sortBy(-_._1).take(10)
//    }
//
//    /**
//      * Finds docs relevant to a doc. Returns the doc IDs and scores for the docs with the highest
//      * relevance scores to the given doc.
//      */
//    def topDocsForDoc(normalizedUS: RowMatrix, docId: Long): Seq[(Double, Long)] = {
//      // Look up the row in US corresponding to the given doc ID.
//      val docRowArr = row(normalizedUS, docId)
//      val docRowVec = Matrices.dense(docRowArr.length, 1, docRowArr)
//
//      // Compute scores against every doc
//      val docScores = normalizedUS.multiply(docRowVec)
//
//      // Find the docs with the highest scores
//      val allDocWeights = docScores.rows.map(_.toArray(0)).zipWithUniqueId
//
//      // Docs can end up with NaN score if their row in U is all zeros.  Filter these out.
//      allDocWeights.filter(!_._1.isNaN).top(10)
//    }
//
//    /**
//      * Finds docs relevant to a term. Returns the doc IDs and scores for the docs with the highest
//      * relevance scores to the given term.
//      */
//    def topDocsForTerm(US: RowMatrix, V: Matrix, termId: Int): Seq[(Double, Long)] = {
//      val termRowArr = row(V, termId).toArray
//      val termRowVec = Matrices.dense(termRowArr.length, 1, termRowArr)
//
//      // Compute scores against every doc
//      val docScores = US.multiply(termRowVec)
//
//      // Find the docs with the highest scores
//      val allDocWeights = docScores.rows.map(_.toArray(0)).zipWithUniqueId
//      allDocWeights.top(10)
//    }
//
//    def termsToQueryVector(terms: Seq[String], idTerms: Map[String, Int], idfs: Map[String, Double])
//    : BSparseVector[Double] = {
//      val indices = terms.map(idTerms(_)).toArray
//      val values = terms.map(idfs(_)).toArray
//      new BSparseVector[Double](indices, values, idTerms.size)
//    }
//
//    def topDocsForTermQuery(US: RowMatrix, V: Matrix, query: BSparseVector[Double])
//    : Seq[(Double, Long)] = {
//      val breezeV = new BDenseMatrix[Double](V.numRows, V.numCols, V.toArray)
//      val termRowArr = (breezeV.t * query).toArray
//
//      val termRowVec = Matrices.dense(termRowArr.length, 1, termRowArr)
//
//      // Compute scores against every doc
//      val docScores = US.multiply(termRowVec)
//
//      // Find the docs with the highest scores
//      val allDocWeights = docScores.rows.map(_.toArray(0)).zipWithUniqueId
//      allDocWeights.top(10)
//    }
//
//    def printTopTermsForTerm(normalizedVS: BDenseMatrix[Double],
//                             term: String, idTerms: Map[String, Int], termIds: Map[Int, String]) {
//      printIdWeights(topTermsForTerm(normalizedVS, idTerms(term)), termIds)
//    }
//
//    def printTopDocsForDoc(normalizedUS: RowMatrix, doc: String, idDocs: Map[String, Long],
//                           docIds: Map[Long, String]) {
//      printIdWeights(topDocsForDoc(normalizedUS, idDocs(doc)), docIds)
//    }
//
//    def printTopDocsForTerm(US: RowMatrix, V: Matrix, term: String, idTerms: Map[String, Int],
//                            docIds: Map[Long, String]) {
//      printIdWeights(topDocsForTerm(US, V, idTerms(term)), docIds)
//    }
//
//    def printIdWeights[T](idWeights: Seq[(Double, T)], entityIds: Map[T, String]) {
//      println(idWeights.map { case (score, id) => (entityIds(id), score) }.mkString(", "))
//    }
//
//  }

package examples

/**
  * Created by spyder on 28/10/16.
  */

import org.apache.spark.ml.feature._
import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.sql.SparkSession

import scala.collection.mutable
object ProofOfConcept {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder().master("local[*]").config("spark.ui.port","4070")
      .appName("Shoten: Latent Semantic Analysis")
      .getOrCreate()

    allStuff(spark)

  }

  def ngram(sparkSession: SparkSession): Unit ={
    val wordDataFrame = sparkSession.createDataFrame(Seq(
      (0, Array("Hi", "I", "heard", "about", "Spark")),
      (1, Array("I", "wish", "Java", "could", "use", "case", "classes")),
      (2, Array("Logistic", "regression", "models", "are", "neat"))
    )).toDF("id", "words")

    val ngram = new NGram().setN(2).setInputCol("words").setOutputCol("ngrams")

    val ngramDataFrame = ngram.transform(wordDataFrame)
    ngramDataFrame.select("ngrams").show(false)
  }
  def localDB(spark:SparkSession): Unit={

    val df=spark.read.parquet("/shoten/output/word2vec/")

   println(df.schema)
    df.sample(true,0.001).rdd.map(
      row=>{
        println(s"row is $row")
        val mapped = row.getAs[mutable.WrappedArray[Int]](1)
        // now we can use it like normal collection
     val res=   mapped.mkString(" == ")
        println(s"made into $res")
        val vect = row.getAs[SparseVector](2)
        println("vector is "+ vect)
      }
    ).count()
  }

  def allStuff(spark:SparkSession): Unit ={
    val sentenceData = spark.createDataFrame(Seq(
      (0, "Hi I heard about Spark"),
      (0, "I wish Java could use case classes"),
      (1, "Logistic regression models are neat"),
      (1, "Hi I Java")
    )).toDF("label", "sentence")

    val tokenizer = new Tokenizer().setInputCol("sentence").setOutputCol("words")
    val wordsData = tokenizer.transform(sentenceData)
    val hashingTF = new HashingTF()
      .setInputCol("words").setOutputCol("rawFeatures")
    val featurizedData = hashingTF.transform(wordsData)


    val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
    val idfModel = idf.fit(featurizedData)
    val rescaledData = idfModel.transform(featurizedData)
    rescaledData.show(false)
    rescaledData.schema

    val word2Vec = new Word2Vec()
      .setInputCol("words")
      .setOutputCol("result")
      .setVectorSize(3)
      .setMinCount(0)
    val model = word2Vec.fit(rescaledData)
    val result = model.transform(rescaledData)

    result.show(false)
    result.schema
  }
}

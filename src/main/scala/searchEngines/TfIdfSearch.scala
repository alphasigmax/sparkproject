//package searchEngines
//
///**
//  * Created by spyder on 24/09/16.
//  */
//
//import breeze.linalg.{DenseMatrix => BDenseMatrix, DenseVector => BDenseVector, SparseVector => BSparseVector}
//import parsers.WikipediaParser._
//import nlp.StanfordNLP._
//import indexers.TFIDFIndexer._
//import org.apache.spark.{SparkConf, SparkContext}
//import org.apache.spark.SparkContext._
//import org.apache.spark.mllib.linalg._
//import org.apache.spark.mllib.linalg.distributed.RowMatrix
//import org.apache.spark.rdd.RDD
//import org.apache.spark.sql.types.{DoubleType, StructType}
//import java.io._
//
//import parsers.WikipediaParser
//
//import scala.collection.Map
//import scala.collection.mutable.ArrayBuffer
//
//object TfIdfSearch {
//
//
//   val numberOfResults=100
//
//  def main(args: Array[String]) {
//    val numTerms = if (args.length > 1) args(1).toInt else 50000
//    val conf = new SparkConf() .setMaster("local[*]").setAppName("Shoten: TF-IDF Search Engine")
//    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
//    val sc = new SparkContext(conf)
//    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
//    val filteredDF = sqlContext.read.parquet("/shoten/output/filtered/")
//
//    val stopWords = sc.broadcast(loadStopWords("/shoten/stopwords_eng.txt")).value
//    val loadedTermDocMatrix = sqlContext.read.textFile("/shoten/output/tfidf/")
//    loadedTermDocMatrix.show(false)
//   // WikipediaParser.documentTermMatrix(filteredDF,stopWords,numTerms, sc)
////get a term.
//      //get the term id
//
//    //get top docs for term
//  }
//
//  /**
//    * Finds docs relevant to a term. Returns the doc IDs and scores for the docs with the highest
//    * relevance scores to the given term.
//    */
//  def topDocsForTerm(US: RowMatrix, V: Matrix, termId: Int): Seq[(Double, Long)] = {
//    val termRowArr = row(V, termId).toArray
//    val termRowVec = Matrices.dense(termRowArr.length, 1, termRowArr)
//
//    // Compute scores against every doc
//    val docScores = US.multiply(termRowVec)
//
//    // Find the docs with the highest scores
//    val allDocWeights = docScores.rows.map(_.toArray(0)).zipWithUniqueId
//    allDocWeights.top(numberOfResults)
//  }
//
//  /**
//    * Selects a row from a matrix.
//    */
//  def row(mat: Matrix, index: Int): Seq[Double] = {
//    val arr = mat.toArray
//    (0 until mat.numCols).map(i => arr(index + i * mat.numRows))
//  }
//
//
//
//
//
//
//
//
//
//
//
//}
